//https://gitlab.com/DiogoDeMendonca/laboratorio-de-programacao-prof.-walace

/*
2.	Que pe�a ao usu�rio para digitar um caractere, e em seguida exiba esse caractere na tela. 
a) exiba como caractere (%c). 
b) exiba como inteiro (%d). 
c) exiba como hexadecimal (%x). 
*/

#include <stdio.h>
#include <locale.h>

int main(){
	setlocale(LC_ALL, "portuguese");

	char caractere = 0;
	
	printf("Digite um Caractere: \n");
	scanf("%[ -~]", &caractere);
	
	system("CLS");
	printf("\a");
	
	printf("Formato Caractere: \t %c \n", caractere);
	
	printf("Formato Inteiro: \t %d \n", caractere);
	
	printf("Formato Hexadecimal: \t %x \n", caractere);

	return 0;
}

//https://gitlab.com/DiogoDeMendonca/laboratorio-de-programacao-prof.-walace

/*
18.
Ler 10 n�meros a serem digitados pelo usu�rio e armazen�-los em um vetor. 
a) Exibir a quantidade de vezes que o n�mero 3 est� presente no vetor.
*/

#include <stdio.h>
#include <locale.h>

int main(){
	setlocale(LC_ALL, "portuguese");

	int i			= 0;
	int j			= 0;
	int cont3		= 0;
	int vetor[2][5] = {};
	
	
	for ( i = 0 ; i < 2 ; i++ ){
		for ( j = 0 ; j < 5 ; j++ ){
			
			printf("Entre com o n�mero para a posi��o [%d][%d]: ", i, j);
			scanf("%d", &vetor[i][j]);
		}		
	}
	
	for ( i = 0 ; i < 2 ; i++ ){
		for ( j = 0 ; j < 5 ; j++ ){
			
			if (vetor[i][j] == 3){
				
				cont3 = cont3 + 1;
			}
		}
	}
	
	printf("\n");
	printf("\t O n�mero 3 foi digitado %d vezes! \n", cont3);

	return 0;
}

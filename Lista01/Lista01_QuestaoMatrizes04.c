//https://gitlab.com/DiogoDeMendonca/laboratorio-de-programacao-prof.-walace

/*
4)
Leia 2 notas de 10 alunos.
N�o � para ler o Nome. 
As notas devem ser armazenadas em uma matriz 2D 
onde a primeira coluna � a nota1, e 
a segunda coluna a nota2. 
Em seguida, exiba a tabela. 
a) Adicione uma terceira coluna a sua matriz, e 
armazene nela a m�dia das 2 notas. 
Aten��o a m�dia deve ser calculada automaticamente pelo seu programa, 
o usu�rio n�o ir� digitar a m�dia, apenas as 2 notas. 
*/

#include <stdio.h>
#include <locale.h>

int main(){
	setlocale(LC_ALL, "portuguese");

	int i 			 = 0;
	int j 			 = 0;
	float nota1		 = 0.0;
	float nota2		 = 0.0;
	float media		 = 0.0;
	float matriz[5][3] = {};


	for ( i = 0 ; i < 5 ; i++ ){
		for ( j = 0 ; j < 2 ; j++ ){	

			printf("Aluno 0%d: - Nota 0%d: ", i+1, j+1);
			scanf("%f", &matriz[i][j]);
			system("CLS");
			printf("\a");

		}
	}

	printf("\tN 01 \t N 02 \t MED\n");
	printf("      =========================\n");

	for ( i = 0 ; i < 5 ; i++ ){
		for ( j = 0 ; j < 1 ; j++ ){	

			matriz[i][j+2] = (matriz[i][j] + matriz[i][j+1]) / 2 ;

			printf("\t%.2f \t %.2f \t %.2f\n", matriz[i][j], matriz[i][j+1], matriz[i][j+2]);
			
		}
	}

	return 0;
}

//https://gitlab.com/DiogoDeMendonca/laboratorio-de-programacao-prof.-walace

/*
3.	Ler duas notas e exibir a m�dia aritm�tica.
*/

#include <stdio.h>
#include <locale.h>

int main(){
	setlocale(LC_ALL, "portuguese");

	float nota1 	= 0.0;
	float nota2 	= 0.0;
	float media		= 0.0;
	
	printf("Digite Nota 01: \n");
	scanf("%f", &nota1);
	
	system("CLS");
	printf("\a");
	
	printf("Digite Nota 02: \n");
	scanf("%f", &nota2);
	
	system("CLS");
	printf("\a\a");
	
	media = (nota1 + nota2) / 2;
	
	printf("Nota 1: \t %.2f \n", nota1);
	
	printf("Nota 2: \t %.2f \n", nota2);
	
	printf("M�dia : \t %.2f \n", media);
	
	return 0;
}

//https://gitlab.com/DiogoDeMendonca/laboratorio-de-programacao-prof.-walace

/*
9.
Uma companhia quer verificar se um empregado est� qualificado para a aposentadoria.
Para estar em condi��es, um dos seguintes requisitos deve ser satisfeito: 
  * Ter no m�nimo 65 anos de idade. 
  * Ter trabalhado, no m�nimo 30 anos. 
  * Ter no m�nimo 60 anos e ter trabalhado no m�nimo 25 anos. 

		Ler os dados:
		o ano de nascimento do empregado e 
		o ano de seu ingresso na companhia.

		O programa dever� escrever a idade e 
		o tempo de trabalho do empregado e 
		a mensagem "Requerer aposentadoria" ou "N�o requerer".

Obs.: Utilize como ano atual o ano de 2011.
*/

#include <stdio.h>
#include <locale.h>

int main(){
	setlocale(LC_ALL, "portuguese");

	int anoNascimento	= 0;
	int anoIngresso		= 0;
	int idade			= 0;
	int tempoTrabalho	= 0;
		
	//Ler os dados:
	//o ano de nascimento do empregado e 
	printf("Digite o Ano de Nascimento do Empregado: \n");
	scanf("%d", &anoNascimento);
	system("CLS");
	printf("\a");
	
	//o ano de seu ingresso na companhia.
	printf("Digite o Ano de Ingresso na Companhia: \n");
	scanf("%d", &anoIngresso);
	system("CLS");
	printf("\a");	

	//PROCESSAMENTO:
	
	idade = 2011 - anoNascimento;
	
	tempoTrabalho = 2011 - anoIngresso;
	
	//O programa dever� escrever a idade e 
	printf("Idade do Empregado: %d Anos.\n", idade);
	
	//o tempo de trabalho do empregado e 
	printf("Tempo de Trabalho do Empregado: %d Ano(s).\n\n", tempoTrabalho);
	
	//a mensagem "Requerer aposentadoria" ou "N�o requerer".

	//* Ter no m�nimo 65 anos de idade.
	//* Ter trabalhado, no m�nimo 30 anos. 
  	//* Ter no m�nimo 60 anos e ter trabalhado no m�nimo 25 anos.
	if (idade >= 65 || tempoTrabalho >= 30 || (idade >= 60 && tempoTrabalho >= 25)){
		printf("Requerer Aposentadoria!");
	} else {
		printf("N�o Requerer!");
	}

	return 0;
}

//https://gitlab.com/DiogoDeMendonca/laboratorio-de-programacao-prof.-walace

/*
7.	Para ler um n�mero inteiro e dizer se ele � impar ou par.
*/

#include <stdio.h>
#include <locale.h>

int main(){
	setlocale(LC_ALL, "portuguese");

	int num = 0;
	
	printf("Digite um N�mero Inteiro: \n");
	scanf("%d", &num);
	
	system("CLS");
	printf("\a");

	printf("Voc� Digitou: %d \n \n" , num);

	if ( num % 2 == 0 ) {
		printf("===============\n");
		printf("=    P A R    =\n");
		printf("===============\n");
	} else {
		printf("===============\n");
		printf("=  I M P A R  =\n");
		printf("===============\n");		
	}
		
	return 0;
}

//https://gitlab.com/DiogoDeMendonca/laboratorio-de-programacao-prof.-walace

/*
8.	Escreva um algoritmo que leia 3 n�meros e diga qual � o menor entre os tr�s.
*/

#include <stdio.h>
#include <locale.h>

int main(){
	setlocale(LC_ALL, "portuguese");

	int num[3] 	= {0, 0, 0};
	int i		= 0;

	for (i ; i < 3 ; i++){
		printf("Digite Um N�mero Inteiro - #%d : \n", i+1);
		scanf("%d", &num[i]);
		system("CLS");
		printf("\a");
	}
	
	printf("\a");
	
	if (num[0] < num[1] && num[0] < num[2]){
		printf("\t %d <<< Este � o Menor N�mero! \n", num[0]);
		printf("\t %d\n", num[1]);
		printf("\t %d\n", num[2]);
	}
	
	if (num[1] < num[0] && num[1] < num[2]){
		printf("\t %d\n", num[0]);
		printf("\t %d <<< Este � o Menor N�mero! \n", num[1]);
		printf("\t %d\n", num[2]);
	}
		
	if (num[2] < num[0] && num[2] < num[1]){
		printf("\t %d\n", num[0]);
		printf("\t %d\n", num[1]);
		printf("\t %d <<< Este � o Menor N�mero! \n", num[2]);
	}	
/*
	int num1 	= 0;
	int num2 	= 0;
	int num3 	= 0;
	int i		= 0;

	printf("Digite Um N�mero Inteiro - #1: \n");
	scanf("%d", &num1);
	
	system("CLS");
	printf("\a");
	
	printf("Digite Um N�mero Inteiro - #2: \n");
	scanf("%d", &num2);
	
	system("CLS");
	printf("\a");
	
	printf("Digite Um N�mero Inteiro - #3: \n");
	scanf("%d", &num3);
	
	system("CLS");
	printf("\a\a");

	if (num1 < num2 && num1 < num3){
		printf("\t %d \t <<< Este � o Menor N�mero! \n", num1);
		printf("\t %d\n", num2);
		printf("\t %d\n", num3);
	}
	
	if (num2 < num1 && num2 < num3){
		printf("\t %d\n", num1);
		printf("\t %d \t <<< Este � o Menor N�mero! \n", num2);
		printf("\t %d\n", num3);
	}
	
	if (num3 < num1 && num3 < num2){
		printf("\t %d\n", num1);
		printf("\t %d\n", num2);
		printf("\t %d \t <<< Este � o Menor N�mero! \n", num3);
	}

	*/		
	return 0;
}

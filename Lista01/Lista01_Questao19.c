//https://gitlab.com/DiogoDeMendonca/laboratorio-de-programacao-prof.-walace

/*
19.
Ler 10 n�meros a serem digitados pelo usu�rio e armazen�-los em um vetor.
a) Diga qual � o maior dos 10 n�meros.
b) Diga qual � o menor dos 10 n�meros.
*/

#include <stdio.h>
#include <locale.h>

int main(){
	setlocale(LC_ALL, "portuguese");

	int i			= 0;
	int menor		= 0;
	int maior		= 0;
	int vetor[10] 	= {};
		
	for ( i = 0 ; i < 10 ; i++ ){
		
		printf("Entre com o n�mero para a posi��o [%d]: ", i);
		scanf("%d", &vetor[i]);

		if ( i == 0 ){
			
			maior = menor = vetor[i];
			
		} else if (vetor[i] > maior){
			
			maior = vetor[i];
			
		} else if (vetor[i] < menor) {
			
			menor = vetor[i];
		}
	}
	
	printf("\n");
	printf("\t MENOR Valor Digitado: %d \n", menor);
	printf("\t MAIOR Valor Digitado: %d \n", maior);

	return 0;
}

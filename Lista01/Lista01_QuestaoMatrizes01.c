//https://gitlab.com/DiogoDeMendonca/laboratorio-de-programacao-prof.-walace

/*
1) Gere uma matriz 3x4 de inteiros, e 
pe�a para o usu�rio do programa preench�-la: 
Em seguida exiba-a separando os elementos da mesma linha por "-" (tra�o) e 
separando as linhas por nova linha. 
*/

#include <stdio.h>
#include <locale.h>

int main(){
	setlocale(LC_ALL, "portuguese");

	int i = 0;
	int j = 0;
	int matriz[3][4] = {};
		
	for ( i = 0 ; i < 3 ; i++ ){
		for ( j = 0 ; j < 4 ; j++ ){	

			printf("Entre com o n�mero para a posi��o [%d][%d]: ", i, j);
			scanf("%d", &matriz[i][j]);
			system("CLS");
			printf("\a");

		}
	}
	
	for ( i = 0 ; i < 3 ; i++ ){	
		printf("\n\t");
		
		for ( j = 0 ; j < 4 ; j++ ){	
			
			printf("%d - ", matriz[i][j]);
		}
		
		printf("\b\b \n");
	}
	
	return 0;
}

//https://gitlab.com/DiogoDeMendonca/laboratorio-de-programacao-prof.-walace

/*
2)
Gere uma matriz 3x4 de inteiros, e 
pe�a para o usu�rio do programa preench�-la. 
Informe a quantidade de vezes que o n�mero 5 foi digitado.  
*/

#include <stdio.h>
#include <locale.h>

int main(){
	setlocale(LC_ALL, "portuguese");

	int i 			 = 0;
	int j 			 = 0;
	int cont 		 = 0;
	int matriz[3][4] = {};
		
	for ( i = 0 ; i < 3 ; i++ ){
		for ( j = 0 ; j < 4 ; j++ ){	

			printf("Entre com o n�mero para a posi��o [%d][%d]: ", i, j);
			scanf("%d", &matriz[i][j]);
			system("CLS");
			printf("\a");

		}
	}
	
	for ( i = 0 ; i < 3 ; i++ ){	
		printf("\n\t");
		
		for ( j = 0 ; j < 4 ; j++ ){	
			
			printf("%d - ", matriz[i][j]);
			
			if (matriz[i][j] == 5){
			
			cont = cont + 1;
			}
		}
		
		printf("\b\b \n");
	}

	printf("\n\tO n�mero 5 foi digitado %d vez(es)! \n", cont);
	
	return 0;
}

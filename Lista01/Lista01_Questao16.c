//https://gitlab.com/DiogoDeMendonca/laboratorio-de-programacao-prof.-walace

/*
16.	Leia um n�mero inteiro e diga se ele � um n�mero primo ou n�o.
*/

#include <stdio.h>
#include <locale.h>

int main(){
	setlocale(LC_ALL, "portuguese");

	int i			= 1;
	int num 		= 0;
	int cont		= 0;
	int resto		= 0;
	int resultado 	= 0;
	
	printf("Digite Um N�mero Inteiro: \n");
	scanf("%d", &num);
	system("CLS");
	printf("\a");
	
	while ( i <= num){
/*		
		resultado = num / i;
		resto = num % i;
		
		printf("%d / %d = %d \n", num, i, resultado);
		printf("%d %% %d = %d \n\n", num, i, resto);
*/		
		if ( num % i == 0){
			
			cont = cont + 1;
		}
		
		i++;
	}	

//	printf("\tPoss�veis Divis�es: %d \n\n", cont);
	
	if (num == 1 || cont == 2){
		printf("\n\t\t%d � Primo! \n", num);
	} else {
		printf("\n\t\t%d N�o � Primo! \n", num);
	}
	
	return 0;
}

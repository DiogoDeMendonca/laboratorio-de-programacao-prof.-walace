//https://gitlab.com/DiogoDeMendonca/laboratorio-de-programacao-prof.-walace

/*
15.
Desenhe na tela uma forma geom�trica utilizando caracteres,
utilize um espa�o entre os caracteres.
Sugest�o de caracteres: X, 0, O.
a) Desenhe um quadrado de tamanho 5x5.
Exemplo:
X X X X X
X X X X X
X X X X X
X X X X X
X X X X X
*/

#include <stdio.h>
#include <locale.h>

int main(){
	setlocale(LC_ALL, "portuguese");

	int i = 0;
	int j = 0;
	char simbolo = {' '};
	char matriz[5][5] = {' '};
	
	printf("Entre Com Um Caractere Para Desenhar Sua Matriz: \n");
	scanf("%[!-~]", &simbolo);
	system("CLS");
	printf("\a");
	
	for ( i = 0 ; i < 5 ; i++ ){
		for ( j = 0 ; j < 5 ; j++ ){	
		
			matriz[i][j] = simbolo;
			
			printf("%c ", matriz[i][j]);
		}
	
		printf("\n");
	}
	
	return 0;
}

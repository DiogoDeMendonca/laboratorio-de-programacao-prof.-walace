//https://gitlab.com/DiogoDeMendonca/laboratorio-de-programacao-prof.-walace

/*
4.	Ler uma temperatura em graus Fahrenheit e apresent�-la convertida em graus Celsius. A f�rmula de convers�o �: C = (F-32)*(5.0/9).
*/

#include <stdio.h>
#include <locale.h>

int main(){
	setlocale(LC_ALL, "portuguese");

	float fahrenheit	= 0.0;
	float celsius 		= 0.0;
	
	printf("Digite a Temperatura em Fahrenheit: \n");
	scanf("%f", &fahrenheit);
	
	system("CLS");
	printf("\a");
	
	celsius = (fahrenheit-32.0) * (5.0/9.0);
		
	printf("%.2f Graus Fahrenheit equivalem a %.2f Graus Celsius. \n", fahrenheit, celsius);
	
	return 0;
}

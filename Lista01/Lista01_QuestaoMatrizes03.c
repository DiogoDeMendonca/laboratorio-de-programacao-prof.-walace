//https://gitlab.com/DiogoDeMendonca/laboratorio-de-programacao-prof.-walace

/*
2)
Gere uma matriz 3x4 de inteiros, e 
pe�a para o usu�rio do programa preench�-la. 
Informe a quantidade de vezes que o n�mero 5 foi digitado.  
*/

#include <stdio.h>
#include <locale.h>

int main(){
	setlocale(LC_ALL, "portuguese");

	int i 			 = 0;
	int j 			 = 0;
	int maior 		 = 0;
	int menor		 = 0;
	int matriz[3][4] = {};
		
	for ( i = 0 ; i < 3 ; i++ ){
		for ( j = 0 ; j < 4 ; j++ ){	

			printf("Entre com o n�mero para a posi��o [%d][%d]: ", i, j);
			scanf("%d", &matriz[i][j]);
			system("CLS");
			printf("\a");

		}
	}
	
	for ( i = 0 ; i < 3 ; i++ ){	
		printf("\n\t");
		
		for ( j = 0 ; j < 4 ; j++ ){	
			
			printf("%d - ", matriz[i][j]);
			
			if ( i == 0 && j == 0 ){
				
				maior = menor = matriz[i][j];
				
			} else if (matriz[i][j] > maior){
				
				maior = matriz[i][j];
				
			} else if (matriz[i][j] < menor) {
				
				menor = matriz[i][j];
			}
		}
		
		printf("\b\b \n");
	}

	printf("\n");
	printf("\t MENOR Valor Digitado: %d \n", menor);
	printf("\t MAIOR Valor Digitado: %d \n", maior);	

	return 0;
}

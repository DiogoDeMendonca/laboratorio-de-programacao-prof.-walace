//https://gitlab.com/DiogoDeMendonca/laboratorio-de-programacao-prof.-walace

/*
12.	A professora mandou que jo�ozinho escrevesse 500 vezes no quadro a frase: "Eu n�o vou mais jogar avi�ezinhos de papel na sala de aula".
a) Fa�a o que a professora pediu utilizando a estrutura for.
b) Fa�a o que a professora pediu utilizando a estrutura while.
*/

#include <stdio.h>
#include <locale.h>

int main(){
	setlocale(LC_ALL, "portuguese");

	int i = 0;

	for ( i = 1 ; i <= 500 ; i++ ){
	
		printf("%d: Eu n�o vou mais jogar avi�ezinhos de papel na sala de aula.\n", i);
		
	}
	
	return 0;
}

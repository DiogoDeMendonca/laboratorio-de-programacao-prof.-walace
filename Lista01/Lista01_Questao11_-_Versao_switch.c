//https://gitlab.com/DiogoDeMendonca/laboratorio-de-programacao-prof.-walace

/*
11.	Fa�a algoritmo que funcione como uma calculadora entre dois n�meros, e 
que possua as 4 opera��es b�sicas: 
soma, subtra��o, divis�o e multiplica��o. 

O programa deve perguntar ao usu�rio qual opera��o ele quer realizar, 
a resposta do usu�rio dever� ser um caractere, '+' se soma, '-' se subtra��o, '*' se multiplica��o, e '/' se divis�o.
Em seguida o programa deve pedir para o usu�rio digitar o primeiro n�mero e depois o segundo.
Como sa�da o programa deve exibir o resultado da opera��o realizada.
a) fa�a utilizando if/else.
b) fa�a utilizando switch/case.
*/

#include <stdio.h>
#include <locale.h>

int main(){
	setlocale(LC_ALL, "portuguese");

	float num1		= 0.0;
	float num2		= 0.0;
	float resultado	= 0.0;
	char operador	= "";

	while (operador != '+' && operador != '-' && operador != '*' && operador != '/'){
	
		//O programa deve perguntar ao usu�rio qual opera��o ele quer realizar, 
		printf("Entre com o sinal para realizar a Opera��o:\n\n");
		
		//a resposta do usu�rio dever� ser um caractere, '+' se soma, '-' se subtra��o, '*' se multiplica��o, e '/' se divis�o.
		printf("\t+ : SOMA\n");
		printf("\t- : SUBTRA��O\n");
		printf("\t* : MULTIPLICA��O\n");
		printf("\t/ : DIVIS�O\n");
	
		scanf("%c", &operador);
		system("CLS");
		printf("\a");
		
	}
	
	//Em seguida o programa deve pedir para o usu�rio digitar o primeiro n�mero e depois o segundo.
	printf("Digite o Primeiro Termo da Opera��o:\n");
	scanf("%f", &num1);
	system("CLS");
	printf("\a");
	
	printf("Digite o Segundo Termo da Opera��o:\n");
	scanf("%f", &num2);
	system("CLS");
	printf("\a");
	
	switch (operador){
			
		case '+':
			resultado = num1 + num2;
			printf("%.2f + %.2f = %.2f \n", num1, num2, resultado);
			break;
		
		case '-':
			resultado = num1 - num2;
			printf("%.2f - %.2f = %.2f \n", num1, num2, resultado);
			break;
		
		case '*':
			resultado = num1 * num2;
			printf("%.2f * %.2f = %.2f \n", num1, num2, resultado);
			break;
	
		case '/':
			resultado = num1 / num2;
			printf("%.2f / %.2f = %.2f \n", num1, num2, resultado);
			break;
						
	}

	return 0;
}

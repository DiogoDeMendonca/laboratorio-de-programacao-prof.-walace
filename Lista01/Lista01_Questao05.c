//https://gitlab.com/DiogoDeMendonca/laboratorio-de-programacao-prof.-walace

/*
5.	Ler a altura e a base de um tri�ngulo e calcular sua �rea. A f�rmula de �rea de um tri�ngulo � A = (base*altura)/2.
*/

#include <stdio.h>
#include <locale.h>

int main(){
	setlocale(LC_ALL, "portuguese");

	float altura	= 0.0;
	float base 		= 0.0;
	float area 		= 0.0;
	
	printf("Digite a Base do Tri�ngulo: \n");
	scanf("%f", &base);
	
	system("CLS");
	printf("\a");

	printf("Digite a Altura do Tri�ngulo: \n");
	scanf("%f", &altura);
	
	system("CLS");
	printf("\a");
	
	area = (base * altura) / 2;
		
	printf("  Base: %.2f \n", base);
	printf("Altura: %.2f \n", altura);
	printf("  �rea: %.2f \n", area);
	
	return 0;
}

//https://gitlab.com/DiogoDeMendonca/laboratorio-de-programacao-prof.-walace

/*
18.
Ler 10 n�meros a serem digitados pelo usu�rio e armazen�-los em um vetor. 
a) Exibir a quantidade de vezes que o n�mero 3 est� presente no vetor.
*/

#include <stdio.h>
#include <locale.h>

int main(){
	setlocale(LC_ALL, "portuguese");

	int i			= 0;
	int cont		= 0;
	int numPesquisa = 3;
	int vetor[10] 	= {};
	
	for ( i = 0 ; i < 10 ; i++ ){
		
		printf("Entre com o n�mero para a posi��o [%d]: ", i);
		scanf("%d", &vetor[i]);
		
		if (vetor[i] == numPesquisa){
			
			cont = cont + 1;
		}
	}
	
	printf("\n");
	printf("\t O n�mero %d foi digitado %d vezes! \n", numPesquisa, cont);

	return 0;
}

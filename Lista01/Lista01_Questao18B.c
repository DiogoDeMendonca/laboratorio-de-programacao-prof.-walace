//https://gitlab.com/DiogoDeMendonca/laboratorio-de-programacao-prof.-walace

/*
18.
b) Pergunte ao usu�rio qual n�mero ele quer pesquisar (no lugar do n�mero 3), e 
diga quantas vezes este n�mero est� presente no vetor.
*/

#include <stdio.h>
#include <locale.h>

int main(){
	setlocale(LC_ALL, "portuguese");

	int i			= 0;
	int cont		= 0;
	int numPesquisa = 0;
	int vetor[10] 	= {};
	
	printf("Qual N�mero Desejar�s Pesquisar? ", i);
	scanf("%d", &numPesquisa);
	system("CLS");
	printf("\a");
	
	for ( i = 0 ; i < 10 ; i++ ){
		
		printf("Entre com o n�mero para a posi��o [%d]: ", i);
		scanf("%d", &vetor[i]);
		
		if (vetor[i] == numPesquisa){
			
			cont = cont + 1;
		}
	}
	
	printf("\n");
	printf("\t O n�mero %d foi digitado %d vez(es)! \n", numPesquisa, cont);

	return 0;
}

//https://gitlab.com/DiogoDeMendonca/laboratorio-de-programacao-prof.-walace

/*
6.	Ler um n�mero e diga se eles est� contido no intervalo entre 10 e 15, onde 10 e 15 tamb�m pertencem ao intervalo.
*/

#include <stdio.h>
#include <locale.h>

int main(){
	setlocale(LC_ALL, "portuguese");

	int num = 0;
	
	printf("Digite um N�mero Inteiro: \n");
	scanf("%d", &num);
	
	system("CLS");
	printf("\a");

	if (num >= 10 && num <= 15){
		printf("%d Pertence ao Intervalo! \n", num);
	} else {
		printf("%d N�o Pertence ao Intervalo! \n", num);
	}
		
	return 0;
}

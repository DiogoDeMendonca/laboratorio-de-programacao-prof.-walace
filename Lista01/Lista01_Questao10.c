//https://gitlab.com/DiogoDeMendonca/laboratorio-de-programacao-prof.-walace

/*
10.	Leia o ano de nascimento de um nadador, calcule sua idade, e classifique-o em uma das categorias: 
  * Infantil A --- de 5 a 7 anos 
  * Infantil B --- de 8 a 10 anos 
  * Juvenil A --- de 11 a 13 anos 
  * Juvenil B --- de 14 a 17 anos 
  * S�nior --- maiores de 17 anos 
Obs.: Utilize como ano atual o ano de 2011.
	O programa deve fornecer uma sa�da do tipo:
	Nadador de idade "idade" � da categoria "categoria"
*/

#include <stdio.h>
#include <locale.h>

int main(){
	setlocale(LC_ALL, "portuguese");

	int anoNascimento	= 0;
	int idade			= 0;
	
	//Leia o ano de nascimento de um nadador;
	printf("Digite o Ano de Nascimento do Nadador: \n");
	scanf("%d", &anoNascimento);
	system("CLS");
	printf("\a");
	
	//Calcule sua idade;
	idade = 2011 - anoNascimento;
	
	printf("Idade: %d ano(s).\n", idade);
	
	//Classifique-o em uma das categorias: 
	switch (idade){
		//* Infantil A --- de 5 a 7 anos 
		case 5:
		case 6:
		case 7: 
			printf("Nadador de idade %d � da categoria Infantil A.\n", idade);
			break;
		
		//* Infantil B --- de 8 a 10 anos 		
		case 8:
		case 9:
		case 10:
			printf("Nadador de idade %d � da categoria Infantil B.\n", idade);
			break;	
		
		//* Juvenil A --- de 11 a 13 anos 
		case 11:
		case 12:
		case 13:
			printf("Nadador de idade %d � da categoria Juvenil A.\n", idade);
			break;
		
		//* Juvenil B --- de 14 a 17 anos
		case 14:
		case 15:
		case 16:
		case 17:
			printf("Nadador de idade %d � da categoria Juvenil B.\n", idade);
			break;	
		
		//* S�nior --- maiores de 17 anos
		default:
			if (idade > 17){
				printf("Nadador de idade %d � da categoria S�nior.\n", idade);
			} else if (idade >= 0 && idade < 5){
				printf("Nadador sem categoria.\n");	
			} else {
				printf("Idade Inv�lida.\n");	
			}
		break;
	}

	return 0;
}

//https://gitlab.com/DiogoDeMendonca/laboratorio-de-programacao-prof.-walace

/*
Fa�a um programa em C para:
Entrada/Sa�da:
1.	Ler um n�mero e exibir a frase: "o n�mero digitado foi: " e logo ap�s esta frase apresentar o valor do n�mero lido.
*/

#include <stdio.h>
#include <locale.h>

int main(){
	setlocale(LC_ALL, "portuguese");

	int num = 0;
	
	printf("Digite um N�mero Inteiro: \n");
	scanf("%d", &num);
	
	system("CLS");
	printf("\a");
	
	printf("O N�mero digitado foi: %d \n", num);

	return 0;
}

//https://gitlab.com/DiogoDeMendonca/laboratorio-de-programacao-prof.-walace

/*
17.
Leia dois n�meros inteiros, 
o primeiro representar� a base e 
o segundo o expoente. 

Calcule o valor da base elevado ao expoente. 

Exemplo: 34 = 3*3*3*3 = 81;  53 = 5*5*5 = 125.
Em C n�o existe nenhum operador matem�tico que calcule o expoente.
Desenvolva o seu pr�prio algoritmo para realizar o c�lculo, utilizando um la�o de repeti��o para realizar sucessivas multiplica��es.
*/

#include <stdio.h>
#include <locale.h>

int main(){
	setlocale(LC_ALL, "portuguese");

	int i			= 0;
	int exp 		= 0;
	int base 		= 0;
	int resultado 	= 0;
	
	printf("Digite Um N�mero Inteiro - BASE: \n");
	scanf("%d", &base);
	getchar();
	system("CLS");
	printf("\a");
	
	printf("Digite Um N�mero Inteiro - EXPOENTE: \n");
	scanf("%d", &exp);
	getchar();
	system("CLS");
	printf("\a");

	if (exp == 0){

		resultado = 1;
		printf("RESULTADO: %d \n", resultado);
		
	} else if (exp == 1){

		resultado = base;
		printf("RESULTADO: %d \n", resultado);
		
	} else if (exp == 2){
		
		resultado = base * base;
		printf("RESULTADO: %d \n", resultado);
		
	} else if (exp > 2){
		
		resultado = base * base;
	
		while ( i < exp - 2 ){
	
			resultado = resultado * base;
			
			i++;
		}
		
		printf("RESULTADO: %d \n", resultado);	
	}	
	
	return 0;
}

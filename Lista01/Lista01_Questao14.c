//https://gitlab.com/DiogoDeMendonca/laboratorio-de-programacao-prof.-walace

/*
14.
Leia dois valores, e 
exiba sua soma.
Em seguida pergunte ao usu�rio:
"Novo C�lculo (S/N)?". 
Deve-se ler a resposta e 
se a resposta for 'S' (sim), 
deve-se repetir todos os comandos (instru��es) novamente, mas 
se for qualquer outra resposta, o algoritmo deve ser finalizado escrevendo a mensagem "Fim dos C�lculos".
*/

#include <stdio.h>
#include <locale.h>

int main(){
	setlocale(LC_ALL, "portuguese");

	float num1		= 0.0;
	float num2		= 0.0;
	float resultado	= 0.0;
	char novoCalculo = 'S';

	while (novoCalculo == 'S' || novoCalculo == 's'){
	
		printf("Digite o Primeiro Termo da Opera��o:\n");
		scanf("%f", &num1);
		system("CLS");
		printf("\a");
		
		printf("Digite o Segundo Termo da Opera��o:\n");
		scanf("%f", &num2);
		getchar();
		system("CLS");
		printf("\a");

		resultado = num1 + num2;
		
		printf("%.2f + %.2f = %.2f \n\n", num1, num2, resultado);
		
		printf("Novo C�lculo (S/N)?\n");
		scanf("%c", &novoCalculo);
		system("CLS");
		printf("\a");
	
	}

	printf("\n\tFim dos C�lculos\n");
	printf("\a");

	return 0;
}

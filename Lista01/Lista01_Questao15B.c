//https://gitlab.com/DiogoDeMendonca/laboratorio-de-programacao-prof.-walace

/*
15.
b) Pergunte ao usu�rio qual o tamanho do quadrado que ele quer que seja desenhado, e o desenhe.
*/

#include <stdio.h>
#include <locale.h>

int main(){
	setlocale(LC_ALL, "portuguese");

	int i 						= 0;
	int j 						= 0;
	int lin 					= 0;
	int col 					= 0;
	char simbolo 				= {'\0'};
	char matriz[100][100] 		= { };
	
	printf("Qual o Tamanho do Quadrado Que Desejas Desenhar? \n");
	scanf("%d", &lin);
	getchar();
	system("CLS");
	printf("\a");

	col = lin;
	
	printf("Entre Com Um Caractere Para Desenhar Sua Matriz: \n");
	scanf("%c", &simbolo);
	getchar();
	system("CLS");
	printf("\a");
	
	for ( i = 0 ; i < lin ; i++ ){
		for ( j = 0 ; j < col ; j++ ){	
		
			matriz[i][j] = simbolo;
			
			printf("%c ", matriz[i][j]);
		}
	
		printf("\n");
	}

	return 0;
}

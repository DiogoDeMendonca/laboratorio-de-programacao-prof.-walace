//https://gitlab.com/DiogoDeMendonca/laboratorio-de-programacao-prof.-walace

/*
13.
Exiba todos os n�meros entre 32 e 126.
Exiba um n�mero por linha, mas 
em cada linha mostre o n�mero em tr�s formatos: 
inteiro (%d), hexadecimal (%x), e caractere (%c).
Utilize a estrutura de repeti��o for na resolu��o do problema.
Compare o resultado com a tabela ascii abaixo.
*/

#include <stdio.h>
#include <locale.h>

int main(){
	setlocale(LC_ALL, "portuguese");

	int i = 0;

	printf("   ==============================\n");
	printf("    =   DEC \t HEX \t CHAR  =\n");
	printf("   ==============================\n");

	//Exiba todos os n�meros entre 32 e 126.
	for ( i = 32 ; i >= 32 && i <= 126 ; i++ ){
	
		//inteiro (%d), hexadecimal (%x), e caractere (%c).
		printf("    |   %d\t %x\t   %c   |\n", i, i, i);
		
	}
	
	printf("   ==============================\n");
	
	return 0;
}
